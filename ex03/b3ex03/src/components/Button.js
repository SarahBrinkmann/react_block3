// When the button (also a separate component) is clicked then you should alert the data collected by the two inputs.

// You should use the `onChange` and `onClick` events 
import React from 'react';


function Button(props) {
    return ( 
          <div>
               <button onClick ={() =>props.showInfo()}>Click me</button>
           </div>
           );
          }

export default Button;

/*In a function component, create a `div` which contains 2 inputs and one button.

Each of these should be a separate child component, all rendered by a parent component called App.
You should collect the data from the inputs and make it available in the parent component (using a function would work for this).

Input 1 should collect the user's email.
Input 2 should collect the user's password.

When the button (also a separate component) is clicked then you should alert the data collected by the two inputs.

> You should use the `onChange` and `onClick` events
*/
import React, { useState } from 'react';
import ReactDom from "react-dom";
import Button from './components/Button.js';
import Email from './components/Email.js';
import Password from './components/Password.js';

function App(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const buttonPressed = () => {
    console.log("Email is;", email);
    console.log("Password is:", password);
  };

const gettingEmail = (eve) => {
 setEmail(eve.target.value);
};

const gettingPassword = (txt) => {
  setPassword(txt);
};

    return (
      <div> 
        <Button showInfo ={()=> buttonPressed()} />
        <Email getEmail= {(e) => gettingEmail(e)} />
        <Password getPassword = {(txt) => gettingPassword(txt)} />
      </div>
    );
  }

export default App;


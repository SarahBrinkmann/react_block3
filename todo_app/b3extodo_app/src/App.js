import React, { useEffect, useState } from 'react';
import Form from './components/Form';
import MyTodoList from './components/MyTodoList';
import './App.css';

const localStorageKey = 'my-todo-list'

function App() {
  const [myTodos, setMyTodos] = useState([]);


  useEffect(() => {
    const myTodosStorage = JSON.parse(localStorage.getItem(localStorageKey));
    if (myTodosStorage) {
      setMyTodos(myTodosStorage);
    }

  }, []);

  useEffect(() => {
    localStorage.setItem(localStorageKey, JSON.stringify(myTodos));
  }, [myTodos]);


  const addTodo=(todo)=> {
    setMyTodos([todo, ...myTodos]);

  }

  const toggleComplete=(id)=> {
    setMyTodos(
      myTodos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          };
        }
        return todo;
      })
    );
  }

  const removeTodo=(id)=> {
    setMyTodos(myTodos.filter(todo => todo.id !==id));
  }

  return (
      <div className="App">
        <div className="todo-icon-container">
          <h1>To-do</h1>
        </div> 
          <Form addTodo={addTodo} />
          <MyTodoList myTodos={myTodos} toggleComplete={toggleComplete} removeTodo={removeTodo}/>
      </div>
      )
}
export default App;

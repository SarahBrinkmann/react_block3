//onChange Event
import React from 'react';

const Email =(props) => {
    return (
            <div>
              <input 
              name= "email"
              onChange ={(e)=>props.getEmail(e.target.value)}
                 type="email"
              />
           </div>
           );
        };

export default Email;

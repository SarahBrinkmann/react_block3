/* Write a class component, with an input that takes the data from the user, the data is then displayed (rendered) as an `h1` in the page.
If no text is provided it should display a text saying "no data provided!" 
> You should use the `onChange` event
*/

//import React from 'react';
//require React 

//class App extends React.Component { //write class component

//handleChange = (e) =>{
 //   let data = e.target.value
 //   console.log(data)
 // };

 // render() {
  //  return (
   //   <div>
    //    <h1 onChange={this.handleChange}>Data provided</h1>
    //    <h1 id ='header'>no data provided!</h1>      
     // </div>
   // );
//};

//export default App;

// onChange Event
import React, { Component } from "react";

class App extends Component {
  state = {
    email: "no data provided!",
  };

  handleChange = event => {

    let value = event.target.value;
    if (value) {
      this.setState({email:value})
    } else {
      this.setState({email:"no data provided!"})
    }
  }

  render() {
    return (
      <>
        <h4>email</h4>
        <input name="email" onChange={this.handleChange} />
        <h1>{this.state.email}</h1> 
      </>
    );
  }
}

export default App;




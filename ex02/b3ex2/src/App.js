
/*## Exercise 2 / Optional with hooks

Refactor the previous exercise, this time using a function component and the `useState` hook. Take a look at `useState` in the "Hooks" block of React part. 
*/


import React, { useState } from "react";

function App() {

  const [email, setEmail] = useState('no data provided!')

  const handleNameChange = (e) => setEmail(e.target.value)
        

    return (
    <div className='main'>
       <form onSubmit={event => {
                event.preventDefault();
                console.log(email);
             }}>

          <div className='child'>email
              <input onChange={handleNameChange}/>
              <p>{email}</p>
          </div>
       </form>
    </div>
  );
}

export default  App


